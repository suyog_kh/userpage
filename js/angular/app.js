'use strict';
var myApp = angular.module('userpage',['ui.router','ngMaterial','ngMessages','ngPageTitle'])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        $urlRouterProvider.otherwise("/");
        $stateProvider
            .state("index", {
                data: {
                    pageTitle: "User Page"
                },
                url: "/",
                templateUrl:"/index.html",
            })
    })
